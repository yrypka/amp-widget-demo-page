(function(w, d, id, src) {
  // ver=1.0&cid=9941da78-699c-11e8-96e9-f91df274544b&bid=choicely-widget-9941da78-699c-11e8-96e9-f91df274544b-71378b385df1&t=1528470727091
  var p = {
    ver: '1.0',
    cid: '9941da78-699c-11e8-96e9-f91df274544b',
    bid: 'choicely-widget-9941da78-699c-11e8-96e9-f91df274544b-71378b385df1',
  };
  var s = d.createElement('script');
  s.async = 1;
  s.id = id;
  s.onload = function() {
    w.ChoicelyWidget.i(p);
  };
  s.src = src;
  var t = function() {
    setTimeout(function() {
      if (w.ChoicelyWidget) {
        clearTimeout(t);
        w.ChoicelyWidget.i(p);
      }
    }, 1000);
  };
  d.getElementById(id) ? t() : d.getElementsByTagName('head')[0].appendChild(s);
})(window, window.document, 'choicely-widget-contest', 'https://widget.choicely.com/js/dist/widget.js?t=' + Date.now());
