const express = require('express');

const host = '127.0.0.1';
const port = 1337;

const app = express();
app.use(express.static(__dirname + '/public'));

app.get('/', function(request, response) {
  response.send('Hello!!');
});

app.listen(port, host);
